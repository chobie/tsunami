<?php
namespace Tsunami;

class Request{
	protected $uri;
	protected $request_method;

	public function normaraizedURI(){
		if(false !== ($offset = strpos($this->uri,"?"))){
			$request = substr($this->uri,0,$offset);
		}else{
			$request = $this->uri;
		}

		return $request;
	}

	public function setPath($path){
		$this->uri = $path;
	}

	public function __construct(){
	}

	public function is_post(){
		return ($this->request_method == "POST") ? true : false;
	}

	public function is_get(){
		return ($this->request_method == "GET") ? true : false;
	}

	public function is_put(){
		return ($this->request_method == "PUT") ? true : false;
	}

}