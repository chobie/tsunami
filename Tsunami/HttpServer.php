<?php
declare(ticks = 1);
namespace Tsunami;

/**
* Libevent Http Server for Sample Application
*
* @author chobi_e
*
* system required.
*   php 5.3 higher.
* 	pecl http
* 		svn co http://svn.php.net/repository/pecl/http/trunk pecl_http
*		pecl libevent
* 		svn co http://svn.php.net/repository/pecl/libevent/trunk pecl_libevent
*
*/
class HttpServer{
	const EAGAIN = 11;
	
	protected $response_str = array(
		"100"=>"Continue",
		"101"=>"Switching Protocols",
		"102"=>"Processing",
		"200"=>"OK",
		"201"=>"Created",
		"202"=>"Accepted",
		"203"=>"Non-Authoritative Information",
		"204"=>"No Content",
		"205"=>"Reset Content",
		"206"=>"Partial Content",
		"207"=>"Multi-Status",
		"226"=>"IM Used",
		"300"=>"Multiple Choices",
		"301"=>"Moved Permanently",
		"302"=>"Found",
		"303"=>"See Other",
		"304"=>"Not Modified",
		"305"=>"Use Proxy",
		"306"=>"(Unused)",
		"307"=>"Temporary Redirect",
		"400"=>"Bad Request",
		"401"=>"Unauthorized",
		"402"=>"Payment Required",
		"403"=>"Forbidden",
		"404"=>"Not Found",
		"405"=>"Method Not Allowd",
		"406"=>"Not Acceptable",
		"407"=>"Proxy Athentication Required",
		"408"=>"Request Timeout",
		"409"=>"Conflict",
		"410"=>"Gone",
		"411"=>"Length Required",
		"412"=>"Precondition Failed",
		"413"=>"Request Entity Too Large",
		"414"=>"Requested-URI Too Long",
		"415"=>"Unsupported Media Type",
		"416"=>"Requested Range Not Satisfiable",
		"417"=>"Expectation Failed",
		"422"=>"Unprocessable Entity",
		"423"=>"Locked",
		"424"=>"Failed Dependency",
		"426"=>"Upgrade Required",
		"500"=>"Internal Server Error",
		"501"=>"Not Implemented",
		"502"=>"Bad Gateway",
		"503"=>"Service Unavailable",
		"504"=>"Gateway Timeout",
		"505"=>"HTTP Version Not Supported",
		"506"=>"Variant Also Negotiates",
		"507"=>"Insufficient Storage",
		"508"=>"Not Extended"
		);
	protected $address;
	protected $port;
	protected $socket;
	protected $application;
	
	protected $number = 0;
	protected $connections = array();
	protected $buffers = array();
	protected $result = array();
	
	public function signal_handler($signo){
		switch($signo){
			case SIGTERM:
				exit;
				break;
			case SIGHUP:
				exit;
				break;
			case SIGUSR1:
				echo "SIGUSR1 caught.";
				break;
			default:
				break;
		}
	}
	
	private function bind($address,$port){
		$socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
		if(is_resource($socket)){
			socket_set_nonblock($socket);
			socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);

			socket_bind($socket,"0.0.0.0",$port);
			socket_listen($socket,8192);

			$this->socket = $socket;
			return true;
		}
		return false;
	}
	
	
	public function error($buffer,$error,$id){
	  event_buffer_disable($this->buffers[$id], EV_READ | EV_WRITE);
	  event_buffer_free($this->buffers[$id]);

	  socket_close($this->connections[$id]);
	  unset($this->buffers[$id], $this->connections[$id],$this->result[$id]);
	}

	public function write($buffer,$id){
	  event_buffer_disable($this->buffers[$id], EV_READ | EV_WRITE);
	  event_buffer_free($this->buffers[$id]);

	  socket_close($this->connections[$id]);
	  unset($this->buffers[$id], $this->connections[$id],$this->result[$id]);
	}
	
	public function read($buffer,$id){
    while ($read = event_buffer_read($buffer, 8192)) {
    	$this->result[$id] .= $read;
    }

    if(($r = strrpos($this->result[$id],"\r\n\r\n")) === false){
    	return;
    }
    
    try{
			$message = new \HttpMessage(ltrim($this->result[$id]));
		}catch(Exception $e){
			ev_error($buffer,$id);
		}

		$_SERVER = $message->getHeaders();
		$_SERVER["REQUEST_URI"] = $message->getRequestUrl();


		$request = clone $this->request;

		$request->setPath($_SERVER['REQUEST_URI']);
		$method = $this->application;

    $response = array();
    $response[0] = "HTTP/1.0 200 OK";
    $response[1][] = "Content-type: text/html; charset='utf8'";

  	unset($this->result[$id]);
		try{
			ob_start();
			$method($request,$response);
	    $data = ob_get_clean();
	    $response[1][] = "";

	    unset($request);
	    unset($message);

	    $response[2] = $data;
			$response[1] = join("\r\n",$response[1]);
			event_buffer_write ($buffer,join("\r\n",$response));
		}catch(NotFoundException $e){
			ob_end_clean();
			event_buffer_write($buffer,join("\r\n",$this->error_404($response)));
		}
	}
	
	private function error_404(Array $response){
		$response[0] = "HTTP/1.0 404 Not Found";
		$response[1] = join("\r\n",$response[1]);
		$response[2] = "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL %%REQUEST_URI%% was not found on this server.</p>\r\n<hr>\r\n<address>%%SERVER_SOFTWARE%% Server at %%HTTP_HOST%% Port %%SERVER_PORT%%</address>\r\n</body></html>";
		return $response;
	}
	
	public function accept($socket, $event, $base){
		if(is_resource($socket)){
			$this->number++;
			if($accept = @socket_accept($socket)){
				//echo "accept connection #{$this->number}\r\n";
				if(is_resource($accept)){
					$this->result[$this->number] = "";
					socket_set_nonblock($accept);
					
					$buffer = event_buffer_new($accept,array($this,"read"),array($this,"write"),array($this,"error"),$this->number);

				  event_buffer_base_set($buffer, $base);
				  event_buffer_timeout_set($buffer, 30, 30);
				  event_buffer_watermark_set($buffer, EV_READ, 0, 0xffffff);
				  event_buffer_priority_set($buffer, 10);
				  event_buffer_enable($buffer, EV_READ | EV_PERSIST);
				 
				  $this->connections[$this->number] = $accept;
				  $this->buffers[$this->number] = $buffer;
				}else{
					echo "Something wrong" . PHP_EOL;
					// something wrong.
				}
			}else{
				$error = socket_last_error();
				if($error != self::EAGAIN){
					echo "[Warning] " . socket_strerror($error) . PHP_EOL;
					@socket_close($socket);
				}
				return;
			}
		}
	}

	public function __construct($application){
		pcntl_signal(SIGTERM, array($this,'signal_handler'),false);
		pcntl_signal(SIGHUP,  array($this,"signal_handler"),false);
		pcntl_signal(SIGUSR1, array($this,"signal_handler"),false);
		echo "[server pid] " . posix_getpid() . PHP_EOL;

		$this->application = $application;
	}
	
	private function io_loop(){
		$base = event_base_new();
		$event = event_new();

		event_set($event, $this->socket, EV_READ | EV_PERSIST, array($this,"accept"),$base);
		event_base_set($event, $base);

		event_add($event);
		event_base_loop($base);
	}
	
	public function listen($address,$port){
		$this->bind($address,$port);
		$this->request = new Request();

		for($i=0;$i<15;$i++){
			$pid = pcntl_fork();
			if($pid == -1){
				die("can't fork");
			}else if($pid){
				
			}else{
        //child
				$this->io_loop();
			}
		}

		$this->io_loop();
	}
}