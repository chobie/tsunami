<?php
namespace Tsunami;

class Application{
	public $cached;
	protected $application;

	public function __construct(Array $application){
		$this->cached = array();
		$this->application = $application;
	}
	
	public function __invoke($request,$response){
		$response = array();
		$path = $request->normaraizedURI();

		if(isset($this->cached[$path])){
			$this->cached[$path]($request);
			return;
		}

		foreach($this->application as $location => $invoke){
			if($location == $request->normaraizedURI()){
				$this->cached[$location] = $invoke;
				$invoke($request,$response);
				return;
			}
		}
		
		throw new NotFoundException();
	}
}


