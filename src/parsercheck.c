#include <http_parser.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

/*
struct http_parser {
  //PRIVATE
  unsigned char type : 2;
  unsigned char flags : 6;
  unsigned char state;
  unsigned char header_state;
  unsigned char index;
  char maybe_ml;

  uint32_t nread;
  int64_t content_length;

  //READ-ONLY
  unsigned short http_major;
  unsigned short http_minor;
  unsigned short status_code; // responses only
  unsigned char method;    //requests only

  //1 = Upgrade header was present and the parser has exited because of that.
  //0 = No upgrade header present.
  //Should be checked when http_parser_execute() returns in addition to
  //error checking.
  //
  char upgrade;

  //PUBLIC
  void *data; // A pointer to get hook to the "connection" or "socket" object
};
*/


struct tsunami_http_header_line{
	char *field;
	size_t field_len;
	char *value;
	size_t value_len;
};

struct tsunami_http_connection
{
	int socket;
	int connected_at;
	// phpの$_SERVER準拠
	char *http_host;
	char *http_connection;
	char *http_accept;
	char *http_user_agent;
	char *http_accept_encoding;
	char *http_accept_language;

	char *path;

	// ここらへんはいらんかな。
	char *server_signature;
	char *server_name;
	char *server_addr;
	int server_port;
	char *document_root;
	char *server_admin;
	char *script_filename;
	int server_protocal;

	char *remote_addr;
	int remote_port;
	int request_method;
	char *query_string;
	char *request_uri;
	char *script_name;
	int request_time;
	
	// parse用
	int last_type;
	int current_header;
	struct sockaddr_in address;
	struct tsunami_http_header_line *lines;
};


int on_message_begin(http_parser *p)
{
	printf("message began!\n");
	struct tsunami_http_connection *c = (struct tsunami_http_connection*)p->data;
	printf("check[%d]",c->socket);
	return 0;
}
int on_headers_complete(http_parser *p)
{
	printf("header complete!\n");
	return 0;
}
int on_message_complete(http_parser *p)
{
	printf("message complete!\n");
	struct tsunami_http_connection *c = (struct tsunami_http_connection*)p->data;
	int i;

	printf("Processed lilnes are %d.",c->current_header);
	printf("Captured http header is...\n");
	for(i =0; i< c->current_header;i++){
		printf("[%s] %s\n",c->lines[i].field, c->lines[i].value);
	}
	return 0;
}

int on_path_cb(http_parser *p, const char *at, size_t len, char partial)
{
	char buf[8192];
	memset(buf,0,8192);

	switch(p->method){
		case HTTP_GET:
			printf("[method is get]\n");
	}

	strncpy(buf,at,len);
	buf[len] = '\0';
	printf("[path:%d] %s\n",len,buf);

	return 0;
}
int on_url_cb(http_parser *p, const char *at, size_t len, char partial)
{
	char buf[8192];
	memset(buf,0,8192);

	strncpy(buf,at,len);
	buf[len] = '\0';
	printf("[url:%d] %s\n",len,buf);

	return 0;
}

int header_field_cb(http_parser *p, const char *at, size_t len,char partial)
{
	char *buf = malloc(sizeof(char)*8192);

	strncpy(buf,at,len);
	//printf("[field:%d]%s\n",len,buf);

	struct tsunami_http_connection *c = (struct tsunami_http_connection*)p->data;
	if(c->last_type == 1){
		c->current_header++;
	}

	c->lines[c->current_header].field = buf;
	c->lines[c->current_header].field_len = len;

	//printf("[field:%d]%s\n",len,c->lines[c->current_header].field);

	c->last_type = 1;

	return 0;
}
int on_fragment_cb(http_parser *p, const char *at, size_t len, char partial)
{
	//printf("[Flagment!]\n");
	return 0;
}
int header_value_cb(http_parser *p, const char *at, size_t len, char partial)
{
	char *buf = malloc(sizeof(char)*8192);
	memset(buf,0,8192);

	strncpy(buf,at,len);

	struct tsunami_http_connection *c = (struct tsunami_http_connection*)p->data;

	c->lines[c->current_header].value = buf;
	c->lines[c->current_header].value_len = len;

	//printf("[value:%d]%s\n",len,buf);
	return 0;
}

int on_query_cb(http_parser *p, const char *at, size_t len, char partial)
{
	char buf[8192];
	memset(buf,0,8192);

	strncpy(buf,at,len);
	printf("[query:%d]%s\n",len,buf);
	return 0;
}


int main(void)
{
	int sock;
	struct sockaddr_in client_addr;
	
	sock = socket(AF_INET,SOCK_STREAM,0);
	struct sockaddr_in addr;
	int on = 1;
	setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(8080);
	addr.sin_addr.s_addr = INADDR_ANY;
	bind(sock,(struct sockaddr*)&addr,sizeof(addr));

	listen(sock,10);
	
	//init
	http_parser_settings settings;
	settings.on_message_begin = on_message_begin;
	settings.on_header_field = header_field_cb;
	settings.on_header_value = header_value_cb;
	settings.on_path = on_path_cb;
	settings.on_url = on_url_cb;
	settings.on_fragment = on_fragment_cb;
	settings.on_query_string = on_query_cb;
	settings.on_body = 0;
	settings.on_headers_complete = on_headers_complete;
	settings.on_message_complete = on_message_complete;
	

	for(;;){
		struct tsunami_http_connection *connection = malloc(sizeof(struct tsunami_http_connection));
		connection->lines = malloc(sizeof(struct tsunami_http_header_line)*40);
		connection->current_header = 0;
		connection->last_type = 0;

		size_t len = sizeof(client_addr);
		connection->socket = accept(sock,(struct sockaddr*)&connection->address,&len);

		http_parser *parser = malloc(sizeof(http_parser));
	  	http_parser_init(parser,HTTP_REQUEST);
	  	parser->data = connection;

		char buf[8192];
		read(connection->socket,buf,8192);
		int parsed = http_parser_execute(parser,&settings,buf,strlen(buf));

		char *wr = "HTTP/1.0 200OK\nContent-type: text/html\n\n<h1>Helo World</h1>";

		write(connection->socket,wr,strlen(wr));
		close(connection->socket);

		free(parser);
		free(connection->lines);
		free(connection);
	}
	close(sock);
/*	
	size_t parsed;
	char *buf = "GET http://ww.chobie.com/uhi?moe=1 HTTP/1.0\nHost: www";
	int pass;

	parsed = http_parser_execute(parser,&settings,buf,strlen(buf));
	buf = ".chobie.com\nUser-Agent: chobie/1.0/\n  Uhi hihi\n\n";

	parsed = http_parser_execute(parser,&settings,buf,strlen(buf));
	
	pass = (parsed == strlen(buf));
	printf("parsed %d\n",parsed);
	printf("strlen %d\n",strlen(buf));
	free(parser);
*/
	return 0;
}
