/*
	tsunami PHP Server

	WSGIで動くPHPサーバーを実装するのです。

	現在のステータス：
		SAPI周りをしっかりと実装する
		→現状オレオレサーバーでPHP動かせるだけの環境なので使いもんになりません

	Licence: MIT Licence
	author: chobi_e<http://twitter.com/chobi_e>

	使い方
	#php-5.3 dir
	./configure --prefix=/opt/php5 --enable-embed --enable-debug --enable-maintainer-zts --enable-shared
	make && make install

	#tsunami source dir
	gcc -ggdb -O0 -o a.out -Wall -Werror -I/opt/php5/include/php/ -I/opt/php5/include/php/Zend/ -I/opt/php5/include/php/TSRM -I/opt/php5/include/php/main -L/opt/php5/lib -I./vendor/http-parser tsunami.c ./vendor/http-parser/http_parser.c -levent -lphp5 

	#execute
	LD_LIBRARY_PATH=/usr/lib:/opt/php5/lib ./a.out

*/
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <sys/sendfile.h>

#include <netinet/in.h>

#include <stdlib.h>
#include <stdio.h>


#include <sys/uio.h>
#include <unistd.h>

#include <fcntl.h>

#include <string.h>

//libevent
#include <event.h>
#include <errno.h>

// mopemope/http-parser
#include <http_parser.h>

#include <sys/mman.h>

#include <assert.h>


#include <sys/wait.h>

// php embed sapi
#include <sapi/embed/php_embed.h>

//mmap
//char *_m;
//int myfd;


//write_ubからソケット指定できなさそうなので。
static int tsunami_target_socket;

// http handler

struct tsunami_http_header_line{
	char *field;
	size_t field_len;
	char *value;
	size_t value_len;
};

struct tsunami_http_connection
{
	int socket;
	int connected_at;
	// phpの$_SERVER準拠
	char *http_host;
	char *http_connection;
	char *http_accept;
	char *http_user_agent;
	char *http_accept_encoding;
	char *http_accept_language;

	char *path;

	// ここらへんはいらんかな。
	char *server_signature;
	char *server_name;
	char *server_addr;
	int server_port;
	char *document_root;
	char *server_admin;
	char *script_filename;
	int server_protocal;

	char *remote_addr;
	int remote_port;
	int request_method;
	char *query_string;
	char *request_uri;
	char *script_name;
	int request_time;
	
	// parse用
	int finished;
	int last_type;
	int current_header;
	struct sockaddr_in address;
	struct tsunami_http_header_line *lines;

	// その他
	struct http_parser_settings *settings;
	struct http_parser *parser;
	struct event *events;
};

void tsunami_connection_destroy(struct tsunami_http_connection *connection)
{
	assert(connection != NULL);

	free(connection->lines);
	free(connection->parser);

	//printf("socket:[%d]\n",connection->socket);
	event_del(connection->events);

	//printf("free:[%d]\n",connection->socket);
	free(connection->events);

	//printf("free finished:[%d]\n",connection->socket);
	if(shutdown(connection->socket,SHUT_RDWR) == -1){
		//perror("shutdown");
	}
	close(connection->socket);
	free(connection);
	connection = NULL;

	return;
}

// ry's http parser のcallback
int on_message_begin(http_parser *p)
{
	//printf("message began!\n");
	//struct tsunami_http_connection *c = (struct tsunami_http_connection*)p->data;
	//printf("check[%d]",c->socket);
	return 0;
}

int on_headers_complete(http_parser *p)
{
	//printf("header complete!\n");
	return 0;
}
int on_message_complete(http_parser *p)
{
	//printf("message complete!\n");


	struct tsunami_http_connection *http = p->data;
	//struct event * ev = (http->events);

	http->finished = 1;

	return 0;
}

int on_path_cb(http_parser *p, const char *at, size_t len, char partial)
{
/*
	char buf[8192];
	memset(buf,0,8192);

	switch(p->method){
		case HTTP_GET:
			printf("[method is get]\n");
	}

	strncpy(buf,at,len);
	buf[len] = '\0';
	printf("[path:%d] %s\n",len,buf);
*/
	return 0;
}
int on_url_cb(http_parser *p, const char *at, size_t len, char partial)
{
/*
	char buf[8192];
	memset(buf,0,8192);

	strncpy(buf,at,len);
	buf[len] = '\0';
	printf("[url:%d] %s\n",len,buf);
*/
	return 0;
}

int header_field_cb(http_parser *p, const char *at, size_t len,char partial)
{
/*
	char *buf = malloc(sizeof(char)*8192);

	strncpy(buf,at,len);
	//printf("[field:%d]%s\n",len,buf);
	struct tsunami_http_connection *c = (struct tsunami_http_connection*)p->data;
	if(c->last_type == 1){
		c->current_header++;
	}

	c->lines[c->current_header].field = buf;
	c->lines[c->current_header].field_len = len;

	//printf("[field:%d]%s\n",len,c->lines[c->current_header].field);

	c->last_type = 1;
*/
	return 0;
}
int on_fragment_cb(http_parser *p, const char *at, size_t len, char partial)
{
	//printf("[Flagment!]\n");
	return 0;
}
int header_value_cb(http_parser *p, const char *at, size_t len, char partial)
{
/*
	char *buf = malloc(sizeof(char)*8192);
	memset(buf,0,8192);

	strncpy(buf,at,len);

	struct tsunami_http_connection *c = (struct tsunami_http_connection*)p->data;

	c->lines[c->current_header].value = buf;
	c->lines[c->current_header].value_len = len;
*/
	//printf("[value:%d]%s\n",len,buf);
	return 0;
}

int on_query_cb(http_parser *p, const char *at, size_t len, char partial)
{
/*
	char buf[8192];
	memset(buf,0,8192);

	strncpy(buf,at,len);
	printf("[query:%d]%s\n",len,buf);
*/
	return 0;
}
// ry's http parser のcallback ここまで



// tsunami serverの実装ここから
typedef struct{
	int socket;
	struct sockaddr_in address;
} tsunami_server_socket;

int tsunami_server_getSocket(tsunami_server_socket *server)
{
	return server->socket;
}

void tsunami_server_setSocket(tsunami_server_socket *server,int socket)
{
	server->socket = socket;
	return;
}

void tsunami_server_initAddr(tsunami_server_socket *server)
{
	server->address.sin_family = AF_INET;
	server->address.sin_port = htons(8080);
	server->address.sin_addr.s_addr = INADDR_ANY;

	return;
}
	
tsunami_server_socket * tsunami_server_new()
{
	int On = 1;
	tsunami_server_socket *server = malloc(sizeof(tsunami_server_socket));
	if(server == 0){
		perror("tsunami_server_socket");
	}

	tsunami_server_setSocket(server,socket(AF_INET,SOCK_STREAM,0));
	setsockopt(tsunami_server_getSocket(server),SOL_SOCKET,SO_REUSEADDR,&On,sizeof(On));
	tsunami_server_initAddr(server);

	return server;
}

void tsunami_server_destroy(tsunami_server_socket *server)
{
	free(server);
	return;
}

void tsunami_server_bind(tsunami_server_socket *server)
{
	bind(tsunami_server_getSocket(server),(struct sockaddr*)&server->address,sizeof(server->address));

	int flag = fcntl(tsunami_server_getSocket(server),F_GETFL,0);
	fcntl(tsunami_server_getSocket(server),F_SETFL,O_NONBLOCK|flag);

	return;
}

void tsunami_server_listen(tsunami_server_socket *server)
{
	listen(tsunami_server_getSocket(server),1024);
}

void tsunami_server_close(tsunami_server_socket *server)
{
	close(tsunami_server_getSocket(server));
}


static int tsunami_php_write(const char *str, unsigned int str_length TSRMLS_DC)
{
	//printf("tsunami_php_write\n");
	// Todo: 
	//   いけてるPHP実行環境がつくれたらちゃんと作る
	//   writevのところらへんとか。

	char *message = "HTTP/1.0 200 OK\nContent-Type: text/html\n\n";
	struct iovec iov_write[2];

	iov_write[0].iov_base = message;
	iov_write[0].iov_len = strlen(message);
	iov_write[1].iov_base = (char *)str;
	iov_write[1].iov_len = str_length;
	//sendfile(http->socket,myfd,0,90);
	//lseek(myfd,0,0);
	int ret = writev(tsunami_target_socket,iov_write,2);
	return ret;
}


void write_response(struct tsunami_http_connection *http){
	//リクエストの内容をみてよしなにPHPを実行して返す。
	// Todo:
	//   APC使わないとクソ重い→ほかの実装を参考にする
	//   環境変数まわりの登録ができるように
	//   その他諸々

	char **argv = NULL;
	int argc = 0;
	void ***tsrm_ls;

	php_embed_module.ub_write = tsunami_php_write;

    php_embed_init(argc, argv PTSRMLS_CC);
    zend_first_try{
    		tsunami_target_socket = http->socket;
    		char *filename = "test.php";
			zend_file_handle zfd;
			zfd.type = ZEND_HANDLE_FILENAME;
			zfd.filename = filename;
			zfd.free_filename = 0;
			zfd.opened_path = NULL;
			//spprintf(&include_script,0,"include '%s';",filename);
            php_execute_script(&zfd TSRMLS_CC);
    } zend_catch {
    	/* int exit_status = EG(exit_status); */
    } zend_end_try();
    php_embed_shutdown(TSRMLS_C);
}

static void accept2_handler(int socket,short event, void *arg)
{
	int vread;
	char buf[8192];
	memset(&buf,0,8192);

	struct tsunami_http_connection *http = (struct tsunami_http_connection*)arg;

	//struct event *ev = (struct event*)http->events;
	http_parser *parser = http->parser;
	http_parser_settings *settings = http->settings;
	//int res;
	int nparsed;

	if(event & EV_READ){
		// * [Todo]: Readしてメッセージをパースしてから出力する
		vread = read(socket,buf,8192);
		//printf("%s\n",buf);
		size_t len = strlen(buf);

		switch(vread){
			case 0:
				// EOF
				nparsed = http_parser_execute(parser,settings,buf,len);

				if(http->finished == 1){
				}else{
					//perror("something wrong2");
				}
				write_response(http);
				tsunami_connection_destroy(http);
				break;
			case -1:
				if(errno == EAGAIN || errno == EWOULDBLOCK){
					break;
				}
				break;
			default:

				nparsed = http_parser_execute(parser,settings,buf,len);
				
				if(nparsed != len){
					perror("something wrong1");
				}
				if(http->finished == 1){
					write_response(http);
					tsunami_connection_destroy(http);
				}

				break;
		}
	}
}

static void accept_handler(int socket,short event, void *arg)
{
	//struct event *ev = (struct event*)arg;
	//struct event ev;
	//printf("[accept]\n");

	if(event & EV_READ){
		//struct sockaddr_in caddr;
		//struct event *evv = (struct event *)arg;
		http_parser_settings *settings = malloc(sizeof(http_parser_settings));
		if(settings == 0){
			perror("settings");
		}

		settings->on_message_begin = on_message_begin;
		settings->on_header_field = header_field_cb;
		settings->on_header_value = header_value_cb;
		settings->on_path = on_path_cb;
		settings->on_url = on_url_cb;
		settings->on_fragment = on_fragment_cb;
		settings->on_query_string = on_query_cb;
		settings->on_body = 0;
		settings->on_headers_complete = on_headers_complete;
		settings->on_message_complete = on_message_complete;

		struct tsunami_http_connection *connection = malloc(sizeof(struct tsunami_http_connection));
		if(connection == 0){
			perror("tsunami_http_connection");
		}
		socklen_t len = sizeof(connection->address);

		http_parser *parser = malloc(sizeof(http_parser));
		if(parser == 0){
			perror("parser");
		}

		connection->lines = malloc(sizeof(struct tsunami_http_header_line)*20);
		if(connection->lines == 0){
			perror("lines");
		}
		connection->current_header = 0;
		connection->last_type = 0;
	  	connection->settings = settings;
	  	connection->parser = parser;

	  	parser->data = connection;

	  	http_parser_init(parser,HTTP_REQUEST);


		connection->socket = accept(socket,(struct sockaddr*)&connection->address,&len);

		if(connection->socket != -1){
			struct event *ev = (struct event*)malloc(sizeof(struct event));
			if(ev == 0){
				perror("tsunami_server_socket");
			}
			

			connection->events = ev;

			event_set(ev,connection->socket,EV_READ|EV_PERSIST,accept2_handler,connection);
			int ret = event_add(ev,NULL);
			if(ret != 0){
				perror("event_add");
			}
		}
	}
}

void tsunami_server_ioloop(tsunami_server_socket *server)
{
	struct event ev;
/*
	int fd;
	
	if((fd = open("./response",O_RDONLY)) == -1){
		perror("open");
		return ;
	}
	struct stat sbuf;
	stat("./response",&sbuf);
	_m = (char *)mmap(NULL,sbuf.st_size,PROT_READ, MAP_SHARED,fd,0);
	if(_m == MAP_FAILED){
		perror("mmap");
		return;
	}
	close(fd);
*/
	event_init();

	event_set(&ev,tsunami_server_getSocket(server),EV_READ|EV_PERSIST,accept_handler,&ev);

	int r = event_add(&ev,NULL);
	if(r != 0){
		perror("event_add");
	}

	//printf("event_method: %s\n",event_get_method());
	event_dispatch();

}
// tsunami serverの実装ここまで



int main(int argc, char *argv[])
{
	tsunami_server_socket * server = tsunami_server_new();
	tsunami_server_bind(server);
	tsunami_server_listen(server);
	

	//myfd = open("/dev/shm/response",O_RDONLY);
	//applicationをロードする
	

	//forkするならここで
	tsunami_server_ioloop(server);

	tsunami_server_close(server);
	tsunami_server_destroy(server);

	return EXIT_SUCCESS;
}