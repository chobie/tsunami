<?php
class Application{
  protected $callback;
  public function __call($name,$args){
    $this->callback = $args[0];
  }
  
  public function run(){
    call_user_func($this->callback);
  }
}

$app = new Application();
$app->{"/"}(function(){
  echo "<h1>Hello World</h1>";
});

$app->run();
