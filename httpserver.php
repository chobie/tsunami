<?php
declare(encoding='utf8');
namespace Tsunami;

require "Tsunami/HttpServer.php";
require "Tsunami/NotFoundException.php";
require "Tsunami/Request.php";
require "Tsunami/Application.php";

$app = new Application(array(
		"/"=>function(\Tsunami\Request $request){
			echo "<h1>Hello World</h1>";
			echo "<a href='/next_link'>next_link</a>";
			
		},
		"/next_link"=>function(\Tsunami\Request $request){
			echo "<h1>Next Link</h1>";
		}
	)
);

$server = new HttpServer($app);
$server->listen("0.0.0.0","8080");
